import os
import pathlib

import numpy as np
from PIL import Image

from .QAnnoView import QAnnoView


class QAnnoController():
    def __init__(self, view=None, classes=[], default_class=0):
        if view is None:
            view = QAnnoView()

        self.current_file_idx = 0
        self.files = []
        self.annos = []
        self.classes = classes
        self.default_class = default_class
        self.is_saved = False

        self.view = view
        self.init_events()
        self.view.run()

    def init_events(self):
        self.view.master.bind('<Left>', self.prev_image)
        self.view.master.bind('<Right>', self.next_image)
        self.view.master.bind('<Control-o>', self.open_folder)
        self.view.master.bind('<Control-s>', self.save_annos)

    def init_annos(self):
        if not self.classes:
            return

        # Set quick bindings
        if len(self.classes) < 10:
            num_binds = len(self.classes)
        else:
            num_binds = 10

        for i in range(num_binds):
            if i == 9:
                i = -1
            key = str(i + 1)
            self.view.master.bind(key, self.set_class)

        self.annos = [self.default_class for i in range(len(self.files))]

    def save_annos(self, event):
        if self.annos:
            np.savetxt("frame_annotations.csv", self.annos, delimiter=',', fmt="%d")

            # Update UI
            class_idx = self.classes[self.annos[self.current_file_idx]]
            status_text = "[{}/{}]  {}".format(self.current_file_idx+1, len(self.files), class_idx)
            self.view.update_class_label(status_text)

    def set_class(self, events):
        self.is_saved = False
        class_idx = int(events.char)
        if class_idx == 0:
            class_idx = 10
        class_idx -= 1
        self.annos[self.current_file_idx] = class_idx
        anno = self.classes[class_idx]
        status_text = "[{}/{}]  {}*".format(self.current_file_idx+1, len(self.files), anno)
        self.view.update_class_label(status_text)

    def prev_image(self, event):
        if len(self.files) == 0:
            return

        if self.current_file_idx == 0:
            self.current_file_idx = len(self.files) - 1
        else:
            self.current_file_idx -= 1

        if self.annos:
            anno = self.classes[self.annos[self.current_file_idx]]
        else:
            anno = ""

        if self.is_saved:
            status_text = "[{}/{}]  {}".format(self.current_file_idx+1, len(self.files), anno)
        else:
            status_text = "[{}/{}]  {}*".format(self.current_file_idx+1, len(self.files), anno)
        img = Image.open(self.files[self.current_file_idx])
        self.view.display_image(img, status_text)

    def next_image(self, event):
        if len(self.files) == 0:
            return

        if self.current_file_idx == len(self.files) - 1:
            self.current_file_idx = 0
        else:
            self.current_file_idx += 1

        if self.annos:
            anno = self.classes[self.annos[self.current_file_idx]]
        else:
            anno = ""

        if self.is_saved:
            status_text = "[{}/{}]  {}".format(self.current_file_idx+1, len(self.files), anno)
        else:
            status_text = "[{}/{}]  {}*".format(self.current_file_idx+1, len(self.files), anno)
        img = Image.open(self.files[self.current_file_idx])
        self.view.display_image(img, status_text)

    def open_folder(self, event):
        path = self.view.open_folder()
        # Some checks on path

        self.init_path(path)

    def init_path(self, path):
        files = pathlib.Path(path).glob("*.bmp")
        files = sorted(files, key=self.sort_files)
        if len(files) > 0:
            self.files = files
            self.current_file_idx = 0
            self.init_annos()
            img = Image.open(files[self.current_file_idx])
            if self.annos:
                anno = self.classes[self.annos[0]]
            else:
                anno = ""
            status_text = "[{}/{}]  {}*".format(self.current_file_idx+1, len(self.files), anno)
            self.view.display_image(img, status_text)

    # TODO: hard-coded value will break if file type is different
    def sort_files(self, path):
        return int(os.path.basename(path)[:-4])
