import tkinter as tk
from tkinter import filedialog
from PIL import ImageTk


class QAnnoView(tk.Frame):
    def __init__(self, master=None):
        if master is None:
            master = tk.Tk()
        self.master = master

        super().__init__(master)

        # Set up window
        self.status_text_id = 0
        self.master.title("Quick Annotation Tool")
        self.master.maxsize(1920, 1080)

        self.pack()
        self.create_widgets()

    def run(self):
        self.mainloop()

    def create_widgets(self):
        self.canvas = tk.Canvas(self, width=1920, height=1080)
        self.canvas.pack()

    def update_class_label(self, status_text):
        self.canvas.itemconfigure(self.status_text_id, text=status_text)

    def display_image(self, img, status_text=""):
        p_img = ImageTk.PhotoImage(img)
        self.canvas.create_image(0, 0, anchor="nw", image=p_img)
        self.canvas.image = p_img
        text_x = 20
        text_y = img.size[1] - 60
        self.status_text_id = self.canvas.create_text(text_x,
                                                      text_y,
                                                      anchor="nw",
                                                      fill="blue",
                                                      font="fixed 30 bold",
                                                      text=status_text)

    def open_folder(self):
        path = filedialog.askdirectory(initialdir="~/",
                                          title="Select folder")
        return path
