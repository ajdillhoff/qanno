import argparse
from qanno.QAnnoController import QAnnoController


def main(args):
    print("Quick Annotation Tool")
    print("  classes: ", args.classes)
    print("  default class: ", args.d)
    print("  output file: ", args.o)

    app = QAnnoController(classes=args.classes, default_class=args.d)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Quick Annotation Tool")
    parser.add_argument("classes", metavar='N', type=str, nargs='+',
                        help="classes")
    parser.add_argument("-d", type=int, default=0,
                        help="index of default class, using order given in class list")
    parser.add_argument("-o", type=str, default="anno.csv")
    args = parser.parse_args()
    main(args)
